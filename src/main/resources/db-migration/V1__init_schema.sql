CREATE SCHEMA IF NOT EXISTS realtor;

CREATE TABLE IF NOT EXISTS realtor.role (
  id          BIGINT PRIMARY KEY NOT NULL,
  name        TEXT                  NOT NULL,
  description TEXT
);
CREATE SEQUENCE IF NOT EXISTS realtor.seq_role START 1 INCREMENT BY 1;

CREATE TABLE IF NOT EXISTS realtor.user (
  id BIGINT PRIMARY KEY NOT NULL,
  login TEXT NOT NULL,
  password TEXT NOT NULL,
  email TEXT NOT NULL,
  first_name TEXT NOT NULL,
  last_name TEXT NOT NULL,
  patronymic_name TEXT,
  role_id BIGINT REFERENCES role(id)
);
CREATE SEQUENCE IF NOT EXISTS realtor.seq_user START 1 INCREMENT BY 1;