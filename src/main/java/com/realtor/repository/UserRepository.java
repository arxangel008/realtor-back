package com.realtor.repository;

import com.realtor.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

    User findByLoginAndPassword(String login, String pass);

    User findById(long id);
}
