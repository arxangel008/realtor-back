package com.realtor.repository;

import com.realtor.entity.Role;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface RoleRepository extends CrudRepository<Role, Long> {

    Role findById(long id);

    Collection<Role> findAll();
}
